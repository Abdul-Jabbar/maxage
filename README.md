# Maxage
Generate Max-Age header values or seconds via simple TimeString queries

[//]: # (## Logo)
[//]: # (## Badges)





## Features
* Generate Max-Age header values from simple TimeString queries
* Get seconds as an integer from simple TimeString queries

Example TimeString: `"2 weeks"`





## Installation
```console
$ npm install maxage
```





## Usage
Load with either `import` or `require`
```javascript
// EcmaScript Modules
import { maxage, getSeconds } from 'maxage';

// CommonJS
const { maxage, getSeconds } = require('maxage');
```

There's also a single type that can be imported directly if needed

```typescript
import type { TimeString } from 'maxage';
```

Then use the maxage or getSeconds functions. Simply:

```javascript
/** getMaxAge */
const maxAge = maxage('1 day') // "Max-Age=86400"
/** getSeconds */
const secs = getSeconds('52 weeks') // 30844800
```





## API Reference
### `maxage(age: TimeString) => string`
The return value will be a string in following format: `'Max-Age=86400'`

In case the `age` value passed is not a valid TimeString, the function will return the Max-Age with a value of `0`.

### `getSeconds(timeString: TimeString) => number`
In case the `timeString` value passed is not a valid TimeString, the function will return `0`.




## Typing
### `TimeString`
**Definition**: `${number}${''|' '}${TimeUnit}`

Where `TimeUnit` can be any of the following:
* Minutes: `'m' | 'min' | 'mins' | 'minute' | 'minutes'`
* Hours: `'h' | 'hr' | 'hrs' | 'hour' | 'hours'`
* Days: `'d' | 'day' | 'days'`
* Weeks: `'w' | 'wk' | 'wks' | 'week' | 'weeks'`

Notice the optional space between `number` and `TimeUnit` to suite your style.
The following example are all valid TimeString values:
* `'1min'`
* `'1 min'`
* `'100wks'`
* `'6 days'`





## Author
The author of Maxage is [Abdul Jabbar](https://gitlab.com/Abdul-Jabbar)

Open to contributions and issue reports.

## License
[MIT](https://gitlab.com/Abdul-Jabbar/maxage/blob/main/LICENSE)
