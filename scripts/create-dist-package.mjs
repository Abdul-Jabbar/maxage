#!/usr/bin/env node
import { copyFileSync, writeFileSync } from 'fs';

writeFileSync('./lib/mjs/package.json', '{"type":"module"}');
writeFileSync('./lib/cjs/package.json', '{"type":"commonjs"}');
copyFileSync('package.json', 'lib/package.json')
copyFileSync('README.md', 'lib/README.md')
copyFileSync('LICENSE', 'lib/LICENSE')
