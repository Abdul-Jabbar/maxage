export * from './getMaxAge';
export * from './getSeconds';
export { getSeconds } from './getSeconds';
export { maxage } from './getMaxAge';
export type { TimeString } from './types';
