import { getSeconds } from '../getSeconds';
import type { TimeString } from '../types';

export function maxage(age: TimeString): string {
	return `Max-Age=${getSeconds(age)}`;
}
