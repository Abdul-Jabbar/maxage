import test from 'ava';
import { getSeconds } from '.';

test('getSeconds - minutes', t => {
	const seconds1 = getSeconds('1m');
	const seconds2 = getSeconds('2min');
	const seconds3 = getSeconds('30mins');
	const seconds4 = getSeconds('100minute');
	const seconds5 = getSeconds('10000minutes');
	t.is(seconds1, 60);
	t.is(seconds2, 60*2);
	t.is(seconds3, 60*30);
	t.is(seconds4, 60*100);
	t.is(seconds5, 60*10000);
});

test('getSeconds - hours', t => {
	const seconds1 = getSeconds('1h');
	const seconds2 = getSeconds('2hr');
	const seconds3 = getSeconds('30hrs');
	const seconds4 = getSeconds('100hour');
	const seconds5 = getSeconds('10000hours');
	t.is(seconds1, 60*60);
	t.is(seconds2, 60*60*2);
	t.is(seconds3, 60*60*30);
	t.is(seconds4, 60*60*100);
	t.is(seconds5, 60*60*10000);
});

test('getSeconds - days', t => {
	const seconds1 = getSeconds('1d');
	const seconds2 = getSeconds('2day');
	const seconds3 = getSeconds('30days');
	t.is(seconds1, 60*60*24);
	t.is(seconds2, 60*60*24*2);
	t.is(seconds3, 60*60*24*30);
});

test('getSeconds - weeks', t => {
	const seconds1 = getSeconds('1w');
	const seconds2 = getSeconds('2wk');
	const seconds3 = getSeconds('30wks');
	const seconds4 = getSeconds('100week');
	const seconds5 = getSeconds('10000weeks');
	t.is(seconds1, 60*60*24*7);
	t.is(seconds2, 60*60*24*7*2);
	t.is(seconds3, 60*60*24*7*30);
	t.is(seconds4, 60*60*24*7*100);
	t.is(seconds5, 60*60*24*7*10000);
});

test('getSeconds - spacing', t => {
	const seconds1 = getSeconds('1 m');
	const seconds2 = getSeconds('2 hr');
	const seconds3 = getSeconds('30 day');
	const seconds4 = getSeconds('100 weeks');
	t.is(seconds1, 60);
	t.is(seconds2, 60*60*2);
	t.is(seconds3, 60*60*24*30);
	t.is(seconds4, 60*60*24*7*100);
});
