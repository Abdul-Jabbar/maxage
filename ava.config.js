export default {
    files: ['src/**/*.test.ts'],
    typescript: {
        rewritePaths: { 'src/': 'lib/' },
        compile: false
    },
    verbose: false,
    cache: false,
    timeout: '10s',
    nodeArguments: ['--es-module-specifier-resolution=node', '--no-warnings']
};
