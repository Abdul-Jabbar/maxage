import test from 'ava';
import { maxage } from '.';

test('getMaxAge - valid TimeString', t => {
	const maxAge1 = maxage('1 day');
	const maxAge2 = maxage('0 weeks');
	const maxAge3 = maxage('99 hours');
	t.is(maxAge1, 'Max-Age=86400');
	t.is(maxAge2, 'Max-Age=0');
	t.is(maxAge3, 'Max-Age=356400');
});

test('getMaxAge - invalid TimeString', t => {
	// @ts-ignore
	const maxAge1 = maxage('1year'); // @ts-ignore
	const maxAge2 = maxage('0weks'); // @ts-ignore
	const maxAge3 = maxage('week');
	t.is(maxAge1, 'Max-Age=0');
	t.is(maxAge2, 'Max-Age=0');
	t.is(maxAge3, 'Max-Age=0');
});
