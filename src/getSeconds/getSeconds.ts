import type { TimeString, TimeUnit } from '../types';

const mins: TimeUnit[] = ['m', 'min', 'mins', 'minute', 'minutes'];
const hrs: TimeUnit[] = ['h', 'hr', 'hrs', 'hour', 'hours'];
const days: TimeUnit[] = ['d', 'day', 'days'];
const weeks: TimeUnit[] = ['w', 'wk', 'wks', 'week', 'weeks'];
const timeUnits: TimeUnit[][] = [mins, hrs, days, weeks];


export function getSeconds(timeString: TimeString): number {
	const [val, unit] = timeString.includes(' ')
		? timeString.split(' ')
		: timeString.split(/(\d+)/).slice(1);
	const time = Number.parseInt(val, 10);
	if (Number.isNaN(time)) { return 0; }
	const multipliers: number[] = [60, 3600, 86400, 604800];
	for (let i = 0; i < timeUnits.length; i++) {
		if (timeUnits[i].includes(unit as TimeUnit)) {
			return time * multipliers[i];
		}
	}
	return 0;
}




