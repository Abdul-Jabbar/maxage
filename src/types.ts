export type TimeUnit =
	| 'm' | 'min' | 'mins' | 'minute' | 'minutes'
	| 'h' | 'hr' | 'hrs' | 'hour' | 'hours'
	| 'd' | 'day' | 'days'
	| 'w' | 'wk' | 'wks' | 'week' | 'weeks'


export declare type TimeString = `${number}${''|' '}${TimeUnit}`
